Here is a list of cryptocurrency price and price charts.

- https://www.coingecko.com/en/price_charts/bitcoin/usd
- https://www.coingecko.com/en/price_charts/ethereum/usd
- https://www.coingecko.com/en/price_charts/litecoin/usd
- https://www.coingecko.com/en/price_charts/ripple/usd
- https://www.coingecko.com/en/price_charts/monero/usd
- https://www.coingecko.com/en/price_charts/nem/usd
- https://www.coingecko.com/en/price_charts/dash/usd

